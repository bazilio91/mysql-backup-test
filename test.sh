#!/bin/bash
MYSQL_ROOT_PASSWORD=root

set -ex
# drop previous data
docker rm -fv mysql-test || true
docker-compose up -d mysql-test
sleep 10 # replace with some sort of pinger

BACKUP_DIR=./test-data
S3_CMD_CONFIG=./s3cfg # ~/.s3cfg by default
TEST_SCHEMA_FILE="$BACKUP_DIR/test.schema.tar.gz"
TEST_DATA_FILE="$BACKUP_DIR/test.data.tar.gz"

# find latest files
SCHEMA_FILE=$(s3cmd -c $S3_CMD_CONFIG ls s3://backup | grep "schema.tar" | sort | tail -n1 | awk '{print $4}')
DATA_FILE=$(s3cmd -c $S3_CMD_CONFIG ls s3://backup | grep "data.tar" | sort | tail -n1 | awk '{print $4}')

s3cmd -c $S3_CMD_CONFIG get $SCHEMA_FILE $TEST_SCHEMA_FILE --force
s3cmd -c $S3_CMD_CONFIG get $DATA_FILE $TEST_DATA_FILE --force

gunzip -c $TEST_SCHEMA_FILE | docker exec -i mysql-test sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"'
gunzip -c $TEST_DATA_FILE | docker exec -i mysql-test sh -c 'exec mysql --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;SET SESSION UNIQUE_CHECKS=0; SET SQL_MODE='TRADITIONAL';" -uroot -p"$MYSQL_ROOT_PASSWORD"'

cat test.sql | docker exec -i mysql-test sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"'
