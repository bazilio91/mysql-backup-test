#!/bin/bash

# keep this options before logging start to prevent leak:
LOG_FILE=/tmp/backup.log
MYSQL_ROOT_PASSWORD=root
SLACK_WEBHOOK_URL="https://hooks.slack.com/services/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"

set -ex

DB_NAME=sakila
BACKUP_DIR=./data
DATE=$(date +%F_%R)
S3_CMD_CONFIG=./s3cfg # ~/.s3cfg by default

error() {
  local parent_lineno="$1"
  local message="$2"
  local code="${3:-1}"
  local slack_message=""

  if [[ -n "$message" ]] ; then
    slack_message="Mysql dump failed. Error on or near line ${parent_lineno}: ${message}; exiting with status ${code}."
  else
    slack_message="Mysql dump failed. Error on or near line ${parent_lineno}; exiting with status ${code}."
  fi

  curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"$slack_message\"}" $SLACK_WEBHOOK_URL
  exit "${code}"
}
trap 'error ${LINENO}' ERR

SCHEMA_FILE="$BACKUP_DIR/$DATE.schema.tar.gz"
DATA_FILE="$BACKUP_DIR/$DATE.data.tar.gz"


docker exec mysql sh -c 'exec mysqldump --no-data --databases $MYSQL_DATABASE -uroot -p"$MYSQL_ROOT_PASSWORD"' | gzip -9 > $SCHEMA_FILE
docker exec mysql sh -c 'exec mysqldump --no-create-info --skip-triggers --skip-extended-insert --compact --databases $MYSQL_DATABASE -uroot -p"$MYSQL_ROOT_PASSWORD"' | gzip -9 > $DATA_FILE

# create bucket if not exists
s3cmd -c $S3_CMD_CONFIG ls s3://backup || s3cmd -c $S3_CMD_CONFIG mb s3://backup

s3cmd -c $S3_CMD_CONFIG put $SCHEMA_FILE $DATA_FILE s3://backup

rm $BACKUP_DIR/*
echo "Done!"
