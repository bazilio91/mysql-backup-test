Requirements:
- s3cmd, jq
- docker
- 9000 port

Run:
```bash
docker-compose up -d
./load-test-db.sh
./backup.sh
./test.sh
```

How to improve:
- Select dump to restore promt.
- Full dump log in slack message
- Per table/db dumps for parallelism if db(s) will grow? Now it is only 1 task for upload/download. 
- More complex tests, for example:
  - check if triggers/functions properly loaded
  - check if data is consistent
  - or any other sql
